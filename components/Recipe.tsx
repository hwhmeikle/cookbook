import NextLink from 'next/link';
import { LinkBox, LinkOverlay } from '@chakra-ui/layout';
import { chakra, Box, Image, useColorModeValue } from '@chakra-ui/react';

export type RecipeProps = {
  id: number;
  title: string;
  image?: string;
  user: {
    name: string;
    email: string;
  } | null;
  content?: string;
  published: boolean;
};

const Recipe: React.FC<{ recipe: RecipeProps }> = ({ recipe }) => {
  const { image, title } = recipe;
  return (
    <LinkBox
      bg={useColorModeValue('#F9FAFB', 'gray.600')}
      w="full"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <Box
        h="100%"
        w="xs"
        bg={useColorModeValue('white', 'gray.800')}
        shadow="lg"
        rounded="lg"
        overflow="hidden"
        mx="auto"
      >
        <Image w="full" h={56} fit="cover" src={image} alt="" />

        <Box py={5} textAlign="center">
          <NextLink href={`/p/${recipe.id}`} passHref>
            <LinkOverlay display="block" fontSize="xl" color={useColorModeValue('gray.800', 'white')} fontWeight="bold">
              {title}
            </LinkOverlay>
          </NextLink>
          <chakra.span fontSize="sm" color={useColorModeValue('gray.700', 'gray.200')}>
            Software Engineer
          </chakra.span>
        </Box>
      </Box>
    </LinkBox>
  );
};

export default Recipe;
