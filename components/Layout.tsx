import { Container } from '@chakra-ui/react';
import { Header } from './Header/Header';

const Layout: React.FC = ({ children }) => (
  <div>
    <Header />
    <Container
      as="main"
      maxW="7xl"
      pb={{ base: '10', lg: '24' }}
      pt={{ base: '8', lg: '16' }}
      px={{ base: '4', md: '8' }}
    >
      {children}
    </Container>
  </div>
);

export default Layout;
