import NextLink from 'next/link';
import { useSession } from 'next-auth/client';
import {
  chakra,
  HStack,
  Flex,
  IconButton,
  useColorModeValue,
  useDisclosure,
  CloseButton,
  VStack,
  Button,
  useColorMode,
  Heading,
  InputGroup,
  Input,
  InputRightElement,
  Container,
} from '@chakra-ui/react';
import { useViewportScroll } from 'framer-motion';

import { AiOutlineMenu, AiOutlineSearch, AiOutlineDownload } from 'react-icons/ai';
import { FaMoon, FaSun } from 'react-icons/fa';
import { useEffect, useRef, useState } from 'react';

import { ImportRecipeDialog } from '../ImportRecipeDialog/ImportRecipeDialog';

export const Header: React.FC = () => {
  const { toggleColorMode: toggleMode } = useColorMode();
  const text = useColorModeValue('dark', 'light');
  const SwitchIcon = useColorModeValue(FaMoon, FaSun);
  const bg = useColorModeValue('white', 'gray.800');
  const ref = useRef(null);
  const [y, setY] = useState(0);
  const { height = 0 } = ref.current ? ref.current.getBoundingClientRect() : {};
  const [session] = useSession();

  const { scrollY } = useViewportScroll();
  useEffect(() => {
    return scrollY.onChange(() => setY(scrollY.get()));
  }, [scrollY]);
  const mobileNav = useDisclosure();

  const MobileNavContent = (
    <VStack
      pos="absolute"
      top={0}
      left={0}
      right={0}
      display={{ base: mobileNav.isOpen ? 'flex' : 'none', md: 'none' }}
      flexDirection="column"
      p={2}
      pb={4}
      m={2}
      bg={bg}
      spacing={3}
      rounded="sm"
      shadow="sm"
    >
      <CloseButton aria-label="Close menu" justifySelf="self-start" onClick={mobileNav.onClose} />
      {session ? <AuthenticatedActions /> : <UnauthenticatedActions />}
    </VStack>
  );
  return (
    <>
      <chakra.header
        ref={ref}
        shadow={y > height ? 'sm' : undefined}
        transition="box-shadow 0.2s"
        bg={bg}
        borderTop="6px solid"
        borderTopColor="brand.400"
        w="full"
        overflowY="hidden"
        borderBottomWidth={2}
        borderBottomColor={useColorModeValue('gray.200', 'gray.900')}
      >
        <Container h="4.5rem" maxW="7xl">
          <Flex w="full" h="full" px="6" alignItems="center" justifyContent="space-between">
            <Flex align="flex-start">
              <NextLink href="/">
                <Heading as="span">Cookbook</Heading>
              </NextLink>
            </Flex>
            <Flex display={{ base: 'none', md: session ? 'flex' : 'none' }}>
              <InputGroup w="full" maxW={{ md: '320px' }}>
                <Input paddingEnd={10} bg="gray.50" placeholder="Search for recipe" />
                <InputRightElement children={<AiOutlineSearch />} pointerEvents="none" />
              </InputGroup>
            </Flex>
            <Flex justify="flex-end" align="center" color="gray.400">
              <HStack spacing="5" display={{ base: 'none', md: 'flex' }}>
                {session ? <AuthenticatedActions /> : <UnauthenticatedActions />}
              </HStack>
              <IconButton
                size="md"
                fontSize="lg"
                aria-label={`Switch to ${text} mode`}
                variant="ghost"
                color="current"
                ml={{ base: '0', md: '3' }}
                onClick={toggleMode}
                icon={<SwitchIcon />}
              />
              <IconButton
                display={{ base: 'flex', md: 'none' }}
                aria-label="Open menu"
                fontSize="20px"
                color={useColorModeValue('gray.800', 'inherit')}
                variant="ghost"
                icon={<AiOutlineMenu />}
                onClick={mobileNav.onOpen}
              />
            </Flex>
          </Flex>
          {MobileNavContent}
        </Container>
      </chakra.header>
    </>
  );
};

const UnauthenticatedActions = () => {
  return (
    <NextLink href="/api/auth/signin">
      <Button as="a" colorScheme="green">
        Log in
      </Button>
    </NextLink>
  );
};

const AuthenticatedActions = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <InputGroup display={{ md: 'none' }} w="full" maxW={{ md: '320px' }}>
        <Input paddingEnd={10} bg="gray.50" placeholder="Search for recipe" />
        <InputRightElement children={<AiOutlineSearch />} pointerEvents="none" />
      </InputGroup>

      <Button
        w={{ base: 'full', md: 'auto' }}
        variant="outline"
        colorScheme="green"
        onClick={onOpen}
        leftIcon={<AiOutlineDownload />}
      >
        Import recipe
      </Button>
      <NextLink href="/api/auth/signout" passHref>
        <Button w={{ base: 'full', md: 'auto' }} as="a" colorScheme="green">
          Log out
        </Button>
      </NextLink>

      <ImportRecipeDialog isOpen={isOpen} onClose={onClose} />
    </>
  );
};
