import Router from 'next/router';
import { useForm } from 'react-hook-form';
import {
  Input,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  FormErrorMessage,
} from '@chakra-ui/react';
import { WarningIcon } from '@chakra-ui/icons';

interface Props {
  isOpen: boolean;
  onClose: () => void;
}

export const ImportRecipeDialog: React.FC<Props> = ({ isOpen, onClose }) => {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm();

  const onSubmit = handleSubmit(async ({ website }) => {
    try {
      const body = { website };
      await fetch('/api/recipe', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body),
      });
      onClose();
      await Router.push('/');
    } catch (error) {
      console.error(error);
    }
  });

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Import Recipe</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          Copy and paste the link to your recipe.
          <form onSubmit={onSubmit}>
            <FormControl isInvalid={!!errors.website} mt={4}>
              <FormLabel>Website</FormLabel>
              <Input placeholder="www.example.com" {...register('website', { required: 'Please enter a website' })} />
              <FormErrorMessage>
                <WarningIcon mr={1} />
                {errors.website?.message}
              </FormErrorMessage>
            </FormControl>
            <ModalFooter px={0}>
              <Button colorScheme="green" loadingText="Importing" variant="solid" type="submit" mr={3}>
                Import
              </Button>
              <Button onClick={onClose} classcolorScheme="green" variant="outline">
                Cancel
              </Button>
            </ModalFooter>
          </form>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};
