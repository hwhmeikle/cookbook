import https from 'https';
import cheerio from 'cheerio';
import { getSession } from 'next-auth/client';
import prisma from '../../../lib/prisma';

// POST /api/recipe
// Required fields in body: title
// Optional fields in body: summary, image, content
export default async function handle(req, res) {
  const { website: url } = req.body;
  const session = await getSession({ req });
  await scrapeRecipe(url, async ({ title, image }) => {
    const result = await prisma.recipe.create({
      data: {
        title,
        image,
        user: { connect: { email: session?.user?.email } },
      },
    });
    // console.log(recipe)
    res.json(result);
  });
}

const scrapeRecipe = (url, cb) => {
  https.get(url, (res) => {
    let html = '';

    res.on('data', (chunk) => {
      html += chunk;
    });

    res.on('end', () => {
      const parser = new Parser();
      const recipe = parser.parseRecipe(html);

      cb(recipe);
    });
  });
};

class Parser {
  parseRecipe(html) {
    const $ = cheerio.load(html);

    const title = $('.wprm-recipe-name').text();
    const image = $('.wprm-recipe-image > img').attr('data-lazy-src');

    return { title, image };
  }
}
