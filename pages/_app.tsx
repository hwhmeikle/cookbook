import { Provider } from 'next-auth/client';
import { AppProps } from 'next/app';
import { ChakraProvider, theme } from '@chakra-ui/react';

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <ChakraProvider theme={theme}>
      <Provider session={pageProps.session}>
        <Component {...pageProps} />
      </Provider>
    </ChakraProvider>
  );
};

export default App;
