import React from 'react';
import { GetServerSideProps } from 'next';
import { getSession, useSession } from 'next-auth/client';
import prisma from '../lib/prisma';

import Layout from '../components/Layout';
import Recipe, { RecipeProps } from '../components/Recipe';
import { Grid } from '@chakra-ui/layout';
import { CallToAction } from '../components/CallToAction/CallToAction';

export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const session = await getSession({ req });

  if (!session) {
    res.statusCode = 403;
    return { props: { recipes: [] } };
  }

  const { recipes } = await prisma.user.findUnique({
    where: {
      email: session.user.email,
    },
    select: {
      recipes: true,
    },
  });

  return { props: { recipes } };
};

type Props = {
  recipes: RecipeProps[];
};

const Blog: React.FC<Props> = (props) => {
  const [session] = useSession();
  return (
    <Layout>
      {session ? (
        <>
          <Grid templateColumns="repeat(auto-fill, minmax(200px, 1fr))" gap={4}>
            {props.recipes.map((recipe) => (
              <Recipe key={recipe.id} recipe={recipe} />
            ))}
          </Grid>
        </>
      ) : (
        <CallToAction />
      )}
    </Layout>
  );
};

export default Blog;
