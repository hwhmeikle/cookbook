import React from 'react';
import { GetServerSideProps } from 'next';
import ReactMarkdown from 'react-markdown';
import Layout from '../../components/Layout';
import Router from 'next/router';
import { RecipeProps } from '../../components/Recipe';
import { useSession } from 'next-auth/client';
import prisma from '../../lib/prisma';

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const recipe = await prisma.recipe.findUnique({
    where: {
      id: Number(params?.id) || -1,
    },
    include: {
      user: {
        select: { name: true, email: true },
      },
    },
  });
  return {
    props: recipe,
  };
};

async function deleteRecipe(id: number): Promise<void> {
  await fetch(`http://localhost:3000/api/recipe/${id}`, {
    method: 'DELETE',
  });
  Router.push('/');
}

async function publishRecipe(id: number): Promise<void> {
  await fetch(`http://localhost:3000/api/publish/${id}`, {
    method: 'PUT',
  });
  await Router.push('/');
}

const Recipe: React.FC<RecipeProps> = (props) => {
  const [session, loading] = useSession();
  if (loading) {
    return <div>Authenticating ...</div>;
  }
  const userHasValidSession = Boolean(session);
  const recipeBelongsToUser = session?.user?.email === props.user?.email;
  let title = props.title;
  if (!props.published) {
    title = `${title} (Draft)`;
  }

  return (
    <Layout>
      <div>
        <h2>{title}</h2>
        <p>By {props?.user?.name || 'Unknown author'}</p>
        <ReactMarkdown source={props.content} />
        {!props.published && userHasValidSession && recipeBelongsToUser && (
          <button onClick={() => publishRecipe(props.id)}>Publish</button>
        )}
        {userHasValidSession && recipeBelongsToUser && <button onClick={() => deleteRecipe(props.id)}>Delete</button>}
      </div>
      <style jsx>{`
        .page {
          background: white;
          padding: 2rem;
        }

        .actions {
          margin-top: 2rem;
        }

        button {
          background: #ececec;
          border: 0;
          border-radius: 0.125rem;
          padding: 1rem 2rem;
        }

        button + button {
          margin-left: 1rem;
        }
      `}</style>
    </Layout>
  );
};

export default Recipe;
